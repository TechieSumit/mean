var ObjectID = require('mongodb').ObjectID;

module.exports = function (app, db) {

  // Post Route
  app.post('/notes', (req, res) => {
    const note = { title: req.body.title, text: req.body.text };
    db.collection('notes').insert(note, (err, postResult) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(postResult.ops[0]);
      }
    });
  });

  // Get Route
  // All Items
  app.get('/notes', (req, res) => {
    const details = {};
    db.collection('notes').find().toArray(function (err, allItems) {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(allItems);
      }
    });
  });
  // Single Item
  app.get('/notes/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    db.collection('notes').findOne(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(item);
      }
    });
  });

  // Update Route
  app.put('/notes/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    const note = { title: req.body.title, text: req.body.text };
    db.collection('notes').update(details, note, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send(note);
      }
    });
  });

  // Delete Route
  app.delete('/notes/:id', (req, res) => {
    const id = req.params.id;
    const details = { '_id': new ObjectID(id) };
    db.collection('notes').remove(details, (err, item) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' });
      } else {
        res.send('Note ' + id + ' deleted!');
      }
    });
  });

};