const noteRoutes = require('./note_routes');
const homeRoute = require('./home_route')
module.exports = function(app, db) {
  noteRoutes(app, db);
  homeRoute(app, db);
};