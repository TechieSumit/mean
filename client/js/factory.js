app.factory('noteService', function ($http) {
  var urlBase = '/notes';
  var _noteService = {};

  _noteService.getNotes = function () {
    return $http.get(urlBase);
  };

  _noteService.createNote = function (note) {
    return $http.post(urlBase, note);
  };

  _noteService.deleteNote = function (id) {
    return $http.delete(urlBase + '/' + id);
  };

  _noteService.updateNote = function (id, note) {
    return $http.put(urlBase + '/' + id, note);
  };

  return _noteService;
});