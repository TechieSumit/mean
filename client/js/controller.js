app.controller('noteCtrl', function ($scope, noteService) {
    $scope.appTitle = "Note App"
    $scope.heading = "Note App";
    $scope.edit = false;

    noteService.getNotes().then(function (data) {
        $scope.notes = data.data;
    });


    $scope.save = function () {
        noteService.createNote({
            "title": $scope.titleInput,
            "text": $scope.noteInput
        }).then(function (data) {
            $scope.notes.push(data.data);
            $scope.titleInput = null;
            $scope.noteInput = null;
        });
    }

    $scope.delete = function (i) {
        noteService.deleteNote($scope.notes[i]._id).then(function (data) {
            if (data.data) {
                $scope.notes.splice(i, 1);
            }
        });
    }

    $scope.update = function () {
        noteService.updateNote($scope.id, {
            "title": $scope.titleInput,
            "text": $scope.noteInput
        }).then(function (data) {
            noteService.getNotes().then(function (data) {
                $scope.notes = data.data;
            });
            $scope.titleInput = null;
            $scope.noteInput = null;
            $scope.edit = false;
        });
    }

    $scope.updateForm = function (i) {
        $scope.edit = true;
        $scope.titleInput = $scope.notes[i].title;
        $scope.noteInput = $scope.notes[i].text;
        $scope.id = $scope.notes[i]._id;
    }

    $scope.back = function () {
        $scope.edit = false;
        $scope.titleInput = null;
        $scope.noteInput = null;
    }
});