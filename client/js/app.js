var app = angular.module('noteApp', ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {

    var homeState = {
      name: 'home',
      url: '',
      templateUrl: '/partials/note.html',
      controller: 'noteCtrl'
    }

    $stateProvider.state(homeState);
    $urlRouterProvider.otherwise('');
  });