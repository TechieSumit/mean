const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const path = require('path');
const db = require('./server/config/db');
const cors = require('cors');
const app = express();

app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'server/views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, 'client')));

const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

MongoClient.connect(db.url, (err, database) => {
    if (err) return console.log(err)
    require('./server/routes')(app, database);
    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
})